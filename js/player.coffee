class Track

  constructor: (data) ->
    ###
    data = {
      src: '/track/url/',     # required
      title: 'track title',
      poster: 'url/to/poster'
      duration: '00:00'
    }
    ###
    @src = data.src
    @title = data.title or @src
    @poster = data.poster or 'images/no-poster.png'
    @duration = data.duration or ''

  render: () ->
    @$element = $('<tr/>',
      'class': 'track',
      'data-src': @src,
      'data-poster': @poster,
      'html': """<td><a href="#">#{@title}</a></td><td>#{@duration}</td>"""
    )
    @$element.data 'track', @
    return @$element


class Playlist

  constructor: (@player) ->
    @tracks = []

  add: (json_track) ->
    track = new Track(json_track)
    @tracks.push track

    # if playlist was rendered - render new track
    if (@$element)
      @$element.append track.render()
    return @

  del: (track_index) ->
    track = @tracks[track_index]
    $(track.element).remove()
    @tracks.splice track_index, 1
    return @

  addList: (json_playlist) ->
    for data in json_playlist
      @add data
    return @

  render: (@element) ->
    @$element = $(@element)
    @$element.empty()

    for track in @tracks
      @$element.append track.render()

    @$element.on 'click', '.track', (e) =>
      @player.playTrack $(e.target).parents('.track').data('track')
    return @


class Player

  constructor: (@element, @settings) ->
    @playlist = new Playlist(@)

    @_audiojs = audiojs.createAll({
      trackEnded: () =>
        @playTrack @nextTrack()
    })[0]

    @_bind_controls()

    @renderPlaylist()

  _bind_controls: () ->
    controls = @element.querySelector('.controls')

    @controls =
      play: $(controls.querySelector('.play'))
      stop: $(controls.querySelector('.stop'))
      pause: $(controls.querySelector('.pause'))
      next: $(controls.querySelector('.next'))

    @controls.play.on 'click', (e) =>
      e.preventDefault()
      @_audiojs.play()

    @controls.stop.on 'click', (e) =>
      e.preventDefault()
      @_audiojs.pause()
      @_audiojs.skipTo(0)

    @controls.pause.on 'click', (e) =>
      e.preventDefault()
      @_audiojs.pause()

    @controls.next.on 'click', (e) =>
      e.preventDefault()
      @playTrack @nextTrack()

    return

  renderPlaylist: () ->
    playlist_element = @element.querySelector('.player-playlist .holder')
    @playlist.addList(@settings.playlist).render(playlist_element)

    @playTrack @playlist.tracks[0]

  playTrack: (track) ->
    ### 
    load and play track
    ###
    @current_track = track
    @display track
    @_audiojs.load track.src
    @_audiojs.play()
    @

  display: (track) ->
    ###
    display track info
    ###
    @element.querySelector('.player-description .text').innerText = track.title
    @element.querySelector('.player-poster img').src = track.poster
    @

  getTracks: () ->
    return @playlist.tracks
  
  nextTrack: () ->
    ### 
    switch to next track 
    ###
    next = @current_track.$element.next()
    if next not in [null, undefined]
      return next.data 'track'
    return @getTracks()[0]
