var Player, Playlist, Track;

Track = (function() {

  function Track(data) {
    /*
        data = {
          src: '/track/url/',     # required
          title: 'track title',
          poster: 'url/to/poster'
          duration: '00:00'
        }
    */
    this.src = data.src;
    this.title = data.title || this.src;
    this.poster = data.poster || 'images/no-poster.png';
    this.duration = data.duration || '';
  }

  Track.prototype.render = function() {
    this.$element = $('<tr/>', {
      'class': 'track',
      'data-src': this.src,
      'data-poster': this.poster,
      'html': "<td><a href=\"#\">" + this.title + "</a></td><td>" + this.duration + "</td>"
    });
    this.$element.data('track', this);
    return this.$element;
  };

  return Track;

})();

Playlist = (function() {

  function Playlist(player) {
    this.player = player;
    this.tracks = [];
  }

  Playlist.prototype.add = function(json_track) {
    var track;
    track = new Track(json_track);
    this.tracks.push(track);
    if (this.$element) {
      this.$element.append(track.render());
    }
    return this;
  };

  Playlist.prototype.del = function(track_index) {
    var track;
    track = this.tracks[track_index];
    $(track.element).remove();
    this.tracks.splice(track_index, 1);
    return this;
  };

  Playlist.prototype.addList = function(json_playlist) {
    var data, _i, _len;
    for (_i = 0, _len = json_playlist.length; _i < _len; _i++) {
      data = json_playlist[_i];
      this.add(data);
    }
    return this;
  };

  Playlist.prototype.render = function(element) {
    var track, _i, _len, _ref,
      _this = this;
    this.element = element;
    this.$element = $(this.element);
    this.$element.empty();
    _ref = this.tracks;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      track = _ref[_i];
      this.$element.append(track.render());
    }
    this.$element.on('click', '.track', function(e) {
      return _this.player.playTrack($(e.target).parents('.track').data('track'));
    });
    return this;
  };

  return Playlist;

})();

Player = (function() {

  function Player(element, settings) {
    var _this = this;
    this.element = element;
    this.settings = settings;
    this.playlist = new Playlist(this);
    this._audiojs = audiojs.createAll({
      trackEnded: function() {
        return _this.playTrack(_this.nextTrack());
      }
    })[0];
    this._bind_controls();
    this.renderPlaylist();
  }

  Player.prototype._bind_controls = function() {
    var controls,
      _this = this;
    controls = this.element.querySelector('.controls');
    this.controls = {
      play: $(controls.querySelector('.play')),
      stop: $(controls.querySelector('.stop')),
      pause: $(controls.querySelector('.pause')),
      next: $(controls.querySelector('.next'))
    };
    this.controls.play.on('click', function(e) {
      e.preventDefault();
      return _this._audiojs.play();
    });
    this.controls.stop.on('click', function(e) {
      e.preventDefault();
      _this._audiojs.pause();
      return _this._audiojs.skipTo(0);
    });
    this.controls.pause.on('click', function(e) {
      e.preventDefault();
      return _this._audiojs.pause();
    });
    this.controls.next.on('click', function(e) {
      e.preventDefault();
      return _this.playTrack(_this.nextTrack());
    });
  };

  Player.prototype.renderPlaylist = function() {
    var playlist_element;
    playlist_element = this.element.querySelector('.player-playlist .holder');
    this.playlist.addList(this.settings.playlist).render(playlist_element);
    return this.playTrack(this.playlist.tracks[0]);
  };

  Player.prototype.playTrack = function(track) {
    /* 
    load and play track
    */
    this.current_track = track;
    this.display(track);
    this._audiojs.load(track.src);
    this._audiojs.play();
    return this;
  };

  Player.prototype.display = function(track) {
    /*
        display track info
    */
    this.element.querySelector('.player-description .text').innerText = track.title;
    this.element.querySelector('.player-poster img').src = track.poster;
    return this;
  };

  Player.prototype.getTracks = function() {
    return this.playlist.tracks;
  };

  Player.prototype.nextTrack = function() {
    /* 
    switch to next track
    */

    var next;
    next = this.current_track.$element.next();
    if (next !== null && next !== (void 0)) {
      return next.data('track');
    }
    return this.getTracks()[0];
  };

  return Player;

})();
